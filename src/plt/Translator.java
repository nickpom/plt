package plt;

public class Translator {
	
	public static final  String NIL = "nil";
	
	private String phrase;
	
	public Translator(String inputPhrase) {
		phrase = inputPhrase;
	}

	public String getPhrase() {
		return phrase;
	}

	public String translate() {
		if(startWithVowel(phrase)) {
			if(phrase.endsWith("y")) {
				return phrase+"nay";
			}else if(endWithVowel(phrase)) {
				return phrase+"yay";
			}else if(!endWithVowel(phrase)) {
				return phrase+"ay";
			}
		}else if(phrase.length()>0){
			int nWord=0;
			
			String finalString= new String();
			String[] words = null;
			Boolean space = null;
			
			if(phrase.contains(" ")) {
				words= phrase.split(" "); 
				nWord=words.length;
				space =true;
			}
			
			if(phrase.contains("-")) {
				words = phrase.split("-");
				nWord=words.length;
				space =false;
			}
			
			
				
			if(nWord>1) {
				int i;
				for(i=0;i<nWord;i++) {
					if(i==1)
						finalString=finalString + consonantTranslation(words[i]);
					else if(space)
						finalString=finalString + consonantTranslation(words[i])+ " ";
					else
						finalString=finalString + consonantTranslation(words[i])+ "-";
				}
				return finalString;
			}else {
				return consonantTranslation(phrase);
			}

		}
		return NIL;
	}
	private boolean startWithVowel(String stringa) {
		return stringa.startsWith("a") || stringa.startsWith("e")|| stringa.startsWith("i")|| stringa.startsWith("o")|| stringa.startsWith("u");

	}
	
	private boolean endWithVowel(String stringa) {
		return stringa.endsWith("a") || stringa.endsWith("e")|| stringa.endsWith("i")|| stringa.endsWith("o")|| stringa.endsWith("u");

	}
	
	private boolean endWithPunctuations(String stringa) {
		return stringa.endsWith(".")|| stringa.endsWith("(")|| stringa.endsWith(")") || stringa.endsWith(",")|| stringa.endsWith("'")|| stringa.endsWith(";")|| stringa.endsWith("!")|| stringa.endsWith("?")|| stringa.endsWith(":");

	}
	private boolean startWithPunctuations(String stringa) {
		return stringa.startsWith(".")|| stringa.startsWith("(")|| stringa.startsWith(")") || stringa.startsWith(",")|| stringa.startsWith("'")|| stringa.startsWith(";")|| stringa.startsWith("!")|| stringa.startsWith("?")|| stringa.startsWith(":");

	}
	
	private String consonantTranslation(String stringa) {
		String primitivePhrase = new String();
		
		String special =new String();
		int count=0;
		int position = -1;
		if(startWithPunctuations(stringa)) {
			position=1;
			special=Character.toString(stringa.charAt(stringa.length()-1));
			stringa=stringa.substring(1);
		}else if(endWithPunctuations(stringa)) {
			position=2;
			special=Character.toString(stringa.charAt(stringa.length()-1));
			stringa=stringa.substring(0,stringa.length()-1);
			
		}
		primitivePhrase = stringa;
		while(!startWithVowel(stringa)) {
			stringa=stringa.substring(1);
			count++;
		}
		stringa = primitivePhrase;
		if(position==1) {
		return special+stringa.substring(count)+ stringa.substring(0,count)+"ay";
		}else if(position==2){
		return stringa.substring(count)+ stringa.substring(0,count)+"ay"+special;
		}
		
		return stringa.substring(count)+ stringa.substring(0,count)+"ay";
	}
}
